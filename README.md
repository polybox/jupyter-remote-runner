# Goal

Our customers using our jupyter-lab want to be able to execute code not on the server itself but on other servers that they own.

We assume that their user would be authorized to connect to the other servers through ssh.

We want the user to be able to select the server they want their notebook to be executed on as shown in the following picture when they create a new notebook. The green overlay beeing the new parts to be developped and installed.

![Remote feature](./remote.jpg)


This is a representation of what we want as architecture

![Remote feature](./docs/schema.png)

# Your Goal
- Create a jupyterExtension
- Add the overlay in launcher and storethe selected server
- Add credential handling
- Create the remote execution core
- Add tests
- Ship to our pip repo then update the docker image with the newly created extension






